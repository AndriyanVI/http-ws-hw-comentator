import {
  createCommentIntroduce,
  createCommentPresentation,
  createCommentRandom,
  createCommentCurrentResults,
  createCommentFinish,
  createCommentResults,
  createCommentAlmostFinish
} from "../helpers/commentHelpers";

/// FACADE
export class CommentService {
  constructor(name, players, io) {
    this.name = name;
    this.players = players;
    this.resultsInterval = 30000;
    this.resultsTimerId;
    this.randomCommentInterval = 20000;
    this.randomCommentTimerId;
    this.io = io;
    this.comments = new Comments();
    this.isGameOver = false;
  }

  send(comment) {
    this.io.in(this.name).emit("NEW_COMMENT", comment);
  }

  introduce() {
    const comment = this.comments.create("introduce");
    this.send(comment);
  }

  startGame() {
    const comment = this.comments.create("presentation", { players: this.players });
    this.send(comment);
    this.randomCommentTimer();
    this.currentResultsTimer();
  }

  randomComment() {
    const comment = this.comments.create("random");
    this.send(comment);
  }

  currentResult() {
    const comment = this.comments.create("currentResults", { players: this.players });
    this.send(comment);
  }

  playerFinish(username) {
    const comment = this.comments.create("finish", { username });
    this.send(comment);
  }

  almostFinish() {
    const comment = this.comments.create("almostFinish", { players: this.players });
    this.send(comment);
  }

  results(players) {
    clearInterval(this.resultsTimerId);
    clearInterval(this.randomCommentTimerId);
    const comment = this.comments.create("results", { players });
    this.send(comment);
  }

  updatePlayers(players) {
    this.players = players;
  }

  currentResultsTimer() {
    const announceCurrentResult = () => {
      this.currentResult();
    };
    this.resultsTimerId = setInterval(announceCurrentResult, this.resultsInterval);
  }

  randomCommentTimer() {
    const randomComment = () => {
      this.randomComment();
    };
    this.randomCommentTimerId = setInterval(randomComment, this.randomCommentInterval);
  }
}

/// FACTORY
class Comments {
  create(type, data) {
    let comment;
    switch (type) {
      case "introduce":
        comment = new commentIntroduce();
        break;
      case "presentation":
        comment = new commentPresentation();
        break;
      case "random":
        comment = new commentRandom();
        break;
      case "currentResults":
        comment = new commentCurrentResults();
        break;
      case "almostFinish":
        comment = new commentAlmostFinish();
        break;
      case "finish":
        comment = new commentFinish();
        break;
      case "results":
        comment = new commentResults();
        break;
    }
    return comment.create(data);
  }
}

class commentIntroduce {
  create() {
    return createCommentIntroduce();
  }
}

class commentPresentation {
  create({ players }) {
    return createCommentPresentation(players);
  }
}

class commentRandom {
  create() {
    return createCommentRandom();
  }
}

class commentCurrentResults {
  create({ players }) {
    return createCommentCurrentResults(players);
  }
}

class commentFinish {
  create({ username }) {
    return createCommentFinish(username);
  }
}

class commentResults {
  create({ players }) {
    return createCommentResults(players);
  }
}

class commentAlmostFinish {
  create({ players }) {
    return createCommentAlmostFinish(players);
  }
}
