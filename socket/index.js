import { addUser, deleteUser } from "./services/userService";
import { displayRoomsForUser, addRoom, updateRooms, updateLeavingRooms } from "./services/roomsService";
import { createRoom, joinRoom, leaveRoom } from "./services/roomService";
import { changeReadyStatus, updateGameProgress } from "./services/gamesService";

export default io => {
  io.on("connection", socket => {
    const username = socket.handshake.query.username;
    const userId = socket.id;
    const { error } = addUser(username, userId);
    if (error) {
      socket.emit("CONNECTION_ERROR", error);
    }
    displayRoomsForUser(socket);
    socket.on("CREATE_ROOM", roomName => {
      const createdRoom = createRoom(roomName, username, socket);
      addRoom(createdRoom, io);
    });
    socket.on("JOIN_ROOM", roomName => {
      const updatedRoom = joinRoom(roomName, username, socket);
      updateRooms(updatedRoom, io);
    });
    socket.on("LEAVE_ROOM", roomName => {
      const updatedRoom = leaveRoom(roomName, username, socket);
      updateRooms(updatedRoom, io);
    });
    socket.on("CHANGE_READY_STATUS", roomName => {
      const updatedRoom = changeReadyStatus(roomName, username, socket, io);
      updateRooms(updatedRoom, io);
    });
    socket.on("UPDATE_GAME_PROGRESS", ({ roomName, updatedProgress, charsLeft }) => {
      updateGameProgress(roomName, username, updatedProgress, charsLeft);
    });
    socket.on("disconnect", () => {
      deleteUser(userId);
      updateLeavingRooms(username, socket, io);
    });
  });
};
