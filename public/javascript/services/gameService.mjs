import {
  renderPrepareToGameView,
  updateText,
  renderGameView,
  updateGameTimer,
  renderRoomPage
} from "../helpers/viewHelper.mjs";
import { gameStartTimer, getText, updateGameText, updateUserProgress } from "../helpers/gameHelper.mjs";
import { renderComment } from "../helpers/viewHelper.mjs";

let gameRoom;
let timeToGameEnd;
let currentСharIndex = 0;
let gameText;
let gameSocket;
let progress;
let isGameOver = false;

const handleGame = event => {
  const isTextUpdated = updateGameText(gameText, currentСharIndex, event.key);
  if (isTextUpdated) {
    currentСharIndex++;
    const charsLeft = gameText.length - currentСharIndex;
    progress = updateUserProgress(gameText, currentСharIndex);
    gameSocket.emit("UPDATE_GAME_PROGRESS", { roomName: gameRoom, updatedProgress: progress, charsLeft });
  }
};

const startGame = () => {
  document.addEventListener("keydown", handleGame);
  renderGameView(timeToGameEnd);
  const updateTimeToGameEnd = () => {
    updateGameTimer(timeToGameEnd);
    timeToGameEnd--;
    if (isGameOver || timeToGameEnd < 0) {
      clearInterval(timerId);
    }
  };
  updateTimeToGameEnd();
  const timerId = setInterval(updateTimeToGameEnd, 1000);
};

export const prepareToGame = (timeToGameStart, roomName, textId, gameTime, socket) => {
  gameRoom = roomName;
  timeToGameEnd = gameTime;
  gameSocket = socket;
  isGameOver = false;
  currentСharIndex = 0;

  renderPrepareToGameView(timeToGameStart);
  gameStartTimer(timeToGameStart, startGame);
  getText(textId).then(text => {
    updateText(text);
    gameText = text;
  });
};

export const endGame = updatedRoom => {
  isGameOver = true;
  document.removeEventListener("keydown", handleGame);
  setTimeout(() => renderRoomPage(updatedRoom), 10000);
};

export const updateComment = comment => renderComment(comment);
