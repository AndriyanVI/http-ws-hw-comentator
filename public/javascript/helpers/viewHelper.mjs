import { createElement, removeClass, addClass } from "./domHelper.mjs";
import { getUsername } from "../helpers/connectionHelper.mjs";

export const hideElement = (...elements) => {
  elements.forEach(element => addClass(element, "display-none"));
};

export const displayElement = (...elements) => {
  elements.forEach(element => removeClass(element, "display-none"));
};

export const renderRoom = (name, users, handleJoinButton) => {
  const roomContainer = createElement({
    tagName: "div",
    className: "room"
  });
  const roomUsers = createElement({ tagName: "p" });
  const roomName = createElement({ tagName: "h3" });
  const roomButton = createElement({ tagName: "button", className: "button--join" });
  roomButton.addEventListener("click", handleJoinButton);

  roomUsers.innerHTML = `${users} user connected`;
  roomName.innerHTML = name;
  roomButton.innerHTML = "Join room";

  roomContainer.append(roomUsers, roomName, roomButton);
  return roomContainer;
};

export const renderRooms = (roomsContainer, rooms) => {
  roomsContainer.innerHTML = "";
  roomsContainer.append(...rooms);
};

const renderRoomUser = user => {
  const username = getUsername();
  const { progress, isReady } = user;
  const userContainer = createElement({
    tagName: "li",
    className: "user"
  });
  const userName = createElement({
    tagName: "p",
    className: "user__title"
  });
  const progressBarContainer = createElement({
    tagName: "div",
    className: "progress-bar__container"
  });
  const progressBar = createElement({
    tagName: "div",
    className: "progress-bar"
  });

  userName.innerHTML = user.username == username ? `${user.username} (you)` : user.username;
  progressBar.style.width = `${progress}%`;
  if (progress == 100) {
    addClass(progressBar, "progress-bar--full");
  }
  progressBarContainer.append(progressBar);

  if (isReady) {
    addClass(userContainer, "user--ready");
  } else {
    removeClass(userContainer, "user--ready");
  }
  userContainer.innerHTML = "";
  userContainer.append(userName, progressBarContainer);
  return userContainer;
};

export const renderRoomUsersList = users => {
  const userList = document.querySelector(".users");
  const usersElements = users.map(user => renderRoomUser(user));
  userList.innerHTML = "";
  userList.append(...usersElements);
};

export const updateReadyButton = changedStatus => {
  const readyButton = document.querySelector(".button--ready");
  readyButton.innerHTML = changedStatus ? "Not ready" : "Ready";
};

export const renderRoomPage = room => {
  const leaveRoomButton = document.querySelector(".button--back");
  const roomHeader = document.querySelector(".game__header");
  const readyButton = document.querySelector(".button--ready");
  const countdown = document.querySelector(".countdown");
  const timer = document.querySelector(".timer");
  const text = document.querySelector(".text");
  const commentator = document.querySelector(".game_commentator");
  const comment = document.querySelector(".comment");

  roomHeader.innerHTML = room.roomName;
  readyButton.innerHTML = "Ready";
  countdown.innerHTML = "";
  timer.innerHTML = "";
  text.innerHTML = "";
  comment.innerHTML = "";

  renderRoomUsersList(room.users);
  displayElement(readyButton, leaveRoomButton);
  hideElement(countdown, timer, text, commentator);
};

export const renderPrepareToGameView = () => {
  const leaveRoomButton = document.querySelector(".button--back");
  const readyButton = document.querySelector(".button--ready");
  const commentator = document.querySelector(".game_commentator");

  hideElement(readyButton, leaveRoomButton);
  displayElement(commentator);
};

export const updateGameStartTimer = timeToGameStart => {
  const gameStartTimer = document.querySelector(".countdown");
  displayElement(gameStartTimer);
  gameStartTimer.innerHTML = timeToGameStart;
};

export const updateText = text => {
  const textContainer = document.querySelector(".text");
  textContainer.innerHTML = text;
};

export const renderGameView = () => {
  const gameStartTimer = document.querySelector(".countdown");
  const textContainer = document.querySelector(".text");

  hideElement(gameStartTimer);
  displayElement(textContainer);
};

export const updateGameTimer = timeToGameEnd => {
  const gameTimer = document.querySelector(".timer");
  displayElement(gameTimer);
  gameTimer.innerHTML = `${timeToGameEnd} seconds left`;
};

export const updateEnteredText = textElements => {
  const textContainer = document.querySelector(".text");
  textContainer.innerHTML = "";
  textContainer.append(...textElements);
};

export const createWinnerList = winners => {
  const winnersContainer = createElement({ tagName: "ol", className: "modal-body" });
  const winnersElements = winners.map(winner => {
    const winnerItem = createElement({ tagName: "li" });
    winnerItem.innerText = winner;
    return winnerItem;
  });
  winnersContainer.append(...winnersElements);
  return winnersContainer;
};

export const renderComment = newComment => {
  const comment = document.querySelector(".comment");
  comment.innerHTML = newComment;
};
